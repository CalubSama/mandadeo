   
    CREATE TABLE merchants (
        merchant_id int NOT NULL,
        merchat_name varchar(250) NOT NULL,
        business_name varchar(250) NOT NULL,
        phone varchar(250) NOT NULL,
        merchant_rfc varchar(250) NOT NULL,
        merchant_email varchar(250) NOT NULL,
        password char NOT NULL,
        merchant_address varchar(250) NOT NULL,
        proof_of_address varchar(250) NOT NULL,
        merchant_image varchar(250) NOT NULL,
        postal_code varchar(13) NOT NULL,
        PRIMARY KEY (merchant_id)
    );

    CREATE TABLE costumers (
        costumer_id int NOT NULL,
        first_name varchar(250) NOT NULL,
        last_name varchar(250) NOT NULL,
        postal_code varchar(250) NOT NULL,
        phone varchar(16) NOT NULL,
        email varchar(250) NOT NULL,
        gender ENUM ("HOMBRE","MUJER","NINGUNO")NOT NULL,
        address varchar(250) NOT NULL,
        second_address varchar(250) NOT NULL,
        birthdate date NOT NULL,
        password char NOT NULL,
        PRIMARY KEY (costumer_id)
    );

    CREATE TABLE admins (
        admin_id int NOT NULL,
        first_name varchar(250) NOT NULL,
        birthdate date NOT NULL,
        admin_image date NOT NULL,
        email varchar(250) NOT NULL,
        password char NOT NULL,
        last_name varchar(250) NOT NULL,
        postal_code varchar(13) NOT NULL,
        phone varchar(13) NOT NULL,
        PRIMARY KEY (admin_id)
    );

    CREATE TABLE carts (
        cart_id int NOT NULL,
        FK_id_costumer int NOT NULL,
        cart_total dpuble(11,2) NOT NULL,
        cart_number int NOT NULL,
        date timestamp NOT NULL,
        PRIMARY KEY (cart_id)
    );

    CREATE TABLE payments (
        payment_id int NOT NULL,
        FK_cart_id int NOT NULL,
        payment_type ENUM("CASH","CREDITCARD","OTHERS") NOT NULL,
        allowed bool NOT NULL,
        FK_billing_info_id int NOT NULL,
        PRIMARY KEY (payment_id)
    );

    CREATE TABLE carts_details (
        cart_detail_id int NOT NULL,
        FK_cart_id int NOT NULL,
        FK_costumer_id int NOT NULL,
        FK_product_id int NOT NULL,
        FK_discount_id int NOT NULL,
        FK_costumer_id int NOT NULL,
        cost_ship double(11,2) NOT NULL,
        Column2 INTEGER NOT NULL,
        PRIMARY KEY (cart_detail_id)
    );

    CREATE TABLE products (
        product_id int NOT NULL,
        FK_id_merchant int NOT NULL,
        product_name varchar(250) NOT NULL,
        price double(11,2) NOT NULL,
        picture varchar(250) NOT NULL,
        varcode varchar(13) NOT NULL,
        unit ENUM("UNIDAD","PAQUETE","CAJA","PESO",OFERTA) NOT NULL,
        status ENUM("ENABLED","DISABLED") NOT NULL,
        PRIMARY KEY (product_id)
    );

    CREATE TABLE comments (
        comment_id int NOT NULL,
        FK_user_id int NOT NULL,
        calification int NOT NULL,
        content varchar(250) NOT NULL,
        FK_merhant_id int NOT NULL,
        FK_order_id int NOT NULL,
        PRIMARY KEY (comment_id)
    );

    CREATE TABLE shop_info (
        shop_info_id int NOT NULL,
        FK_merchants_id int NOT NULL,
        shop_name varchar(250) NOT NULL,
        shop_address varchar(250) NOT NULL,
        RFC varchar(16) NOT NULL,
        shop_phone varchar(13) NOT NULL,
        shop_image varchar(250) NOT NULL,
        shop_type ENUM("ABARROTES","MINISUPER","CARNICERIA","MISCELANIA","EXPENDIO","OTROS") NOT NULL,
        PRIMARY KEY (shop_info_id)
    );

    CREATE TABLE personal_info (
        personal_info_id int NOT NULL,
        FK_merchant_id int NOT NULL,
        FK_user_id int NOT NULL,
        FK_admin_id int NOT NULL,
        PRIMARY KEY (personal_info_id)
    );

    CREATE TABLE billing (
        billing_id int NOT NULL,
        FK_user_id int NOT NULL,
        credit_card_id int NOT NULL,
        credit_card_exp date NOT NULL,
        credit_card_pin varchar(6) NOT NULL,
        bill_date timestamp NOT NULL,
        security_code varchar(3) NOT NULL,
        PRIMARY KEY (billing_id)
    );

    CREATE TABLE orders (
        order_id int NOT NULL,
        FK_user_id int NOT NULL,
        order_number int NOT NULL,
        FK_merchant_id int NOT NULL,
        FK_cart_id int NOT NULL,
        order_date timestamp NOT NULL,
        folio_number int NOT NULL,
        PRIMARY KEY (order_id)
    );

    CREATE TABLE order_details (
        order_detail_id int NOT NULL,
        FK_order_id int NOT NULL,
        date_order timestamp NOT NULL,
        FK_cart_id int NOT NULL,
        Column1 INTEGER NOT NULL,
        PRIMARY KEY (order_detail_id)
    );

    CREATE TABLE category (
        category_id int NOT NULL,
        FK_products_id int NOT NULL,
        status ENUM("ENABLED","DISABLED") NOT NULL,
        category_name ENUM("CARNES","CARNESFRIAS","LACTEOS","PANADERIA","REFRESCOS","GOLOSINAS","DERIVADOSDEHARINAS","HIGIENEPERSONAL","ENLATADOS","FRITURAS","OFERTAS","PAPELERIA","LICORES") NOT NULL,
        description varchar(250) NOT NULL,
        PRIMARY KEY (category_id)
    );

    CREATE TABLE discount (
        discount_id int NOT NULL,
        percent_value ENUM("10%","15%","20%","25%","30%","NONE"),
        free_ship bool NOT NULL,
        date_available timestamp NOT NULL,
        description_discount varchar(250) NOT NULL,
        code_of_cupon varchar(10) NOT NULL,
        PRIMARY KEY (discount_id)
    );

    CREATE TABLE  advertisings (
        advertisngs_id int NOT NULL,
        shop_type ENUM("ABARROTES","MINISUPER","CARNICERIA","MISELANIA","EXPENDIO","OTROS") NOT NULL,
        enabled_time datetime NOT NULL,
        disabled_time datetime NOT NULL,
        PRIMARY KEY (advertisngs_id)
    );












