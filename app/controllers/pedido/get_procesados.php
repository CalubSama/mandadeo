<?php
include('../../models/_conexion.php');
$response=new stdClass();

function estado2texto($id){
	switch ($id) {
		case '1':
			return 'Por procesar';
			break;
		case '2':
			return 'Por pagar';
			break;
		case '3':
			return 'Por entregar';
			break;
		case '4':
			return 'En camino';
			break;			
		case '5':
			return 'Entregado';
			break;
		default:
			break;
	}
}

$datos=[];
$i=0;
$sql="select *,ped.estado from pedido ped INNER JOIN costumers c on c.costumer_id=codusu inner join products pro on ped.codpro=pro.codpro where ped.paystatus='PAGADO'";
$result=mysqli_query($con,$sql);
while($row=mysqli_fetch_array($result)){
	$obj=new stdClass();
	$obj->codped=$row['codped'];
	$obj->codpro=$row['codpro'];
	$obj->nompro=utf8_encode($row['nompro']);
	$obj->prepro=$row['prepro'];
	$obj->rutimapro=$row['rutimapro'];
	$obj->fecped=$row['fecped'];
	$obj->costumer_id=$row['costumer_id'];
	$obj->fecped=$row['fecped'];
	$obj->address=utf8_encode($row['address']);
	$obj->phone=$row['phone'];
	$obj->paystatus=$row['paystatus'];
	$obj->estadoped=estado2texto($row['estadoped']);
	$datos[$i]=$obj;
	$i++;
}
$response->datos=$datos;

mysqli_close($con);
header('Content-Type: application/json');
echo json_encode($response);
