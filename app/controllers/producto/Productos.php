<?php
//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";
require_once "Marca.php";
require_once "Categoria.php";

class Producto
{
    //miembros
    //props
    private $id;
    private $nombre;
    private $descripcion;
    private $precio;
    private $codBarras;
    private $imagenes;
    private $marca;
    private $categoria;
    private $unidadDeMedida;
    private $esPerecedero;
    //ob de libBD
    private $bd;
    //constructor conex
    public function __construct($id, $nom, $descrip, $precio, $cod, $imgs, $marca = null, $categoria = null)
    {
        $this->id = $id;
        $this->nombre = $nom;
        $this->descripcion = $descrip;
        $this->precio = $precio;
        $this->codBarras = $cod;
        $this->imagenes = $imgs;
        $this->marca = $marca != null ? $marca : new Marca();
        $this->categoria = $categoria;
        //crea conexion
        $this->bd = new BDLib();
    }
    //destructor!?!??!  desconex
    public function __destruct()
    {
    }
    //métodos

    //CRUDS------------------------
    public function insertar()
    {
        //INSERT INTO productos (nombre, descripcion, precio) VALUES ('doritos', 'bolsa...', 20.19)
        //los datos de los campos a guardar
        $campos = "nombre,descripcion, precio,marca_id, categoria_id, codigo_barras,imagen_producto, unidad_medida, es_perecedero";

        $valores = "'" . $this->getNombre() . "','" . $this->getDescripcion() . "','" . $this->getPrecio() . "','" . $this->getMarca() . "','" . $this->getCategoria() . "','" . $this->getCodBarras() . "','" . $this->getImagenes() . "','" . $this->getUnidadDeMedida() . "'," . $this->getEsPerecedero() . "";
        //"nombre, 'descripcion', 'precio','marca_id', 'categoria_id', 'codigo_barras','imagen_producto', 'unidad_medida', 'es_perecedero'";

        return $this->bd->insertarDevolviendoID('productos', $campos, $valores);
    }
    public function update($datosActuales, $id)
    {

        return $this->bd->actualizar('productos', $datosActuales, $id);
    }
    public function eliminar($id)
    {
        return $this->bd->eliminar("productos", $id);
    }

    public function consultaPorNombre($nombre, $descrip)
    {
    }
    public function consultaPorPrecio($nombre, $descrip)
    {
    }
    //métodos
    public function consultarTodos()
    {
        return $this->bd->consultarTodos('productos');
    }

    public function consultaPorID($id)
    {
        return $this->bd->consultarPorID('productos', '*', $id);
    }

    public function getNombreDeMarca($idMarca)
    {
        return $this->bd->consultarPorID('marcas', 'nombre', $idMarca)[0][0];
    }

    public function getNombreDeCategoria($idCatego)
    {
        return $this->bd->consultarPorID('categorias', 'nombre', $idCatego)[0][0];
    }


    public function consultarPorCriterios($criterios)
    {
        return $this->bd->consultarConArrayCriterios('productos', '*', $criterios);
    }

    public function actualizarPrecios($listaPreciosNuevos)
    {
    }
    public function actualizarPrecio($precioNuevo, $id)
    {
    }

 