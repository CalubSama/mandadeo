<?php

//incluimos la LibBD
require_once "../db/libDB.php";
require_once "../db/CriteriosDeBusqueda.php";

class Usuario
{
    //props de Usuario de pdv UTH 
    private $costumerId;
    private $firstName;
    private $lastName;
    private $postalCode;
    private $phone;
    private $email;
    private $gender;
    private $address;
    private $seconAddress;
    private $birthDate;
    private $password;  
    private $bd;

    //construct
    public function __construct($id, $name, $lName, $pc, $phone, $mail, $gender, $correo, $pass)
    {
        //inicializa props
        $this->costumerId = $id;
        $this->firstName = $name;
        $this->lastName = $lName;
        $this->postalCode = $pc;
        $this->phone = $phone;
        $this->email = $mail;
        $this->gender = $gender;
        $this->address = $address;
        $this->seconAddress = $secAddress;
        $this->birthDate = $birth;
        $this->password = $pass;
        //crea conexion
        $this->bd = new BDLib();
    }

    //CRUDSSS

    //login
    public function login($costumrEmail, $pass)
    {
        

        //hacer consulta
        $hash_password= hash('sha256', $password); //encryptar pass
        $criterios = [
            "0" => new CriterioDeBusqueda('correo', CriterioDeBusqueda::OP_IGUAL, $correo, true, CriterioDeBusqueda::OP_LOGICO_AND),
            "1" => new CriterioDeBusqueda('pwd', CriterioDeBusqueda::OP_IGUAL, $pass, true, CriterioDeBusqueda::OP_LOGICO_NONE),
        ];
        //SELECT * FROM usuarios WHERE correo = '??' AND pwd = '??' ..... 
        //consulta
        return $this->bd->consultarConArrayCriterios('usuarios', '*', $criterios);
    }
    public function userRegistration($username,$password,$email,$name)
    {
        try{
        $db = getDB();
        $st = $db->prepare("SELECT uid FROM users WHERE username=:username OR email=:email"); 
        $st->bindParam("username", $username,PDO::PARAM_STR);
        $st->bindParam("email", $email,PDO::PARAM_STR);
        $st->execute();
        $count=$st->rowCount();
        if($count<1)
        {
        $stmt = $db->prepare("INSERT INTO users(username,password,email,name) VALUES (:username,:hash_password,:email,:name)");
        $stmt->bindParam("username", $username,PDO::PARAM_STR) ;
        $hash_password= hash('sha256', $password); //Password encryption
        $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
        $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
        $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
        $stmt->execute();
        $uid=$db->lastInsertId(); // Last inserted row id
        $db = null;
        $_SESSION['uid']=$uid;
        return true;
        }
        else
        {
        $db = null;
        return false;
        }

        } 
        catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
        }

        /* User Details */
        public function userDetails($uid)
        {
        try{
        $db = getDB();
        $stmt = $db->prepare("SELECT email,username,name FROM users WHERE uid=:uid"); 
        $stmt->bindParam("uid", $uid,PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_OBJ); //User data
        return $data;
        }
        catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    /**
     * @return mixed
     */
    public function getCostumerId()
    {
        return $this->costumerId;
    }

    /**
     * @param mixed $costumerId
     *
     * @return self
     */
    public function setCostumerId($costumerId)
    {
        $this->costumerId = $costumerId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     *
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     *
     * @return self
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     *
     * @return self
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeconAddress()
    {
        return $this->seconAddress;
    }

    /**
     * @param mixed $seconAddress
     *
     * @return self
     */
    public function setSeconAddress($seconAddress)
    {
        $this->seconAddress = $seconAddress;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     *
     * @return self
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     *
     * @return self
     */
    public function setBd($bd)
    {
        $this->bd = $bd;

        return $this;
    }
}
