<?php
	session_start();
	if (!isset($_SESSION['codusu'])) {
		header('location: index.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mandadeo</title>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Sen&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
</head>
<body>
	<header>
		<div class="logo-place"><a href="index.php"><img src="../assets/propuesta2.logoblanco.png"></a></div>
		<div class="search-place">
			<input type="text" id="idbusqueda" placeholder="Encuenta todo lo que necesitas...">
			<button class="btn-main btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
		</div>
		<div class="options-place">
			<?php
			if (isset($_SESSION['codusu'])) {
				echo
				'<div class="item-option"><i class="fa fa-user-circle-o" aria-hidden="true"></i><p>'.$_SESSION['nomusu'].'</p></div>';
			}else{
			?>
			<div class="item-option" title="Registrate"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
			<div class="item-option" title="Ingresar"><i class="fa fa-sign-in" aria-hidden="true"></i></div>
			<?php
			}
			?>
			<div class="item-option" title="Mis compras">
				<a href="carrito.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
			</div>
		</div>
	</header>
	<div class="main-content">
		<div class="content-page" >
			<h1 style="color: white;">Lista de pedidos</h1>
			<div class="body-pedidos" id="space-list">
			</div>

		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
				url:'../../controllers/pedido/merchGet_porprocesar.php',
				type:'POST',
				data:{},
				success:function(data){
					console.log(data);
					let html='';
					let sumaMonto=0;
					let Total=0;
					for (var i = 0; i < data.datos.length; i++) {
						html+=
						'<div class="item-pedido" style="color:back;">'+
							'<div class="pedido-img">'+
								'<img src="../assets/products/'+data.datos[i].rutimapro+'" style="max-height: 216px;">'+
							'</div>'+
							'<div class="pedido-detalle">'+
								'<h3><b>Nombre del cliente:  </b>'+data.datos[i].first_name+'</h3>'+
								'<h3><b>Dirección:</b> '+data.datos[i].address+'</h3>'+
								'<h3>'+data.datos[i].nompro+'</h3>'+
								'<p><b>Fecha:</b> '+data.datos[i].fecped+'</p>'+
								'<p><b>Estado:</b> '+data.datos[i].estadoped+'</p>'+
								'<p><b>Precio:</b> Mxn/ '+data.datos[i].prepro+'</p>'+
								'<p><b>Celular:</b> '+data.datos[i].phone+'</p>'+
							'</div>'+
						'</div>';
						sumaMonto+=parseInt(data.datos[i].prepro)+1;
					}
					total=sumaMonto+35;
						html+='<div class="item-pedido" style="color:back;">'+
							'<div class="pedido-detalle"style="text:center;">'+
								'<div>'+
									'<h3>Numero de articulos: '+data.datos.length+' Articulos</h3>'+
									'<h3>Subtotal: '+sumaMonto+'mxn</h3>'+
									'<h3>Cargo de envio: +35mxn</h3>'+'<h3>Total a cobrar: '+total+'mxn</h3>'+
								'</div>'+					
							'</div>'+
						'</div>';
				    Culqi.settings({
				        title: 'Mandadeo',
				        currency: 'PEN',
				        description: 'Productos convenecia',
				        amount: sumaMonto
				    });
					document.getElementById("space-list").innerHTML=html;
				},
				error:function(err){
					console.error(err);
				}
			});
		});
		function culqi() {
			if (Culqi.token) { 
		      	var token = Culqi.token.id;
		      	$.ajax({
					url:'../../controllers/pedido/merchConfirm.php',
					type:'POST',
					data:{
						dirusu:document.getElementById("dirusu").value,
						telusu:$("#telusu").val(),
						tipopago:2,
						token:token
					},
					success:function(data){
						console.log(data);
						if (data.state) {
							window.location.href="pedido.php";
						}else{
							alert(data.detail);
						}
					},
					error:function(err){
						console.error(err);
					}
				});
		  	} else {
		      	console.log(Culqi.error);
		      	alert(Culqi.error.user_message);
		  	}
		};
	</script>
	<script src="https://checkout.culqi.com/js/v3"></script>
	<script>
	    Culqi.publicKey = 'pk_test_3adf22bd8acf4efc';
	</script>
</body>
</html>