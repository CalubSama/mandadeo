<?php
	session_start();
	unset($_SESSION['nombreImg']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mandadeo la comodidad del mandado</title>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Sen&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
</head>
<body>
	<header>
		<div class="logo-place"><a href="index.php"><img src="../assets/logosencillo.blanco.png"></a></div>
		<div class="search-place" >

			<input type="text" id="idbusqueda" name="idbusqueda" placeholder="Encuenta todo lo que necesitas...">
			<button type="submit" class="btn-main btn-search" ><i class="fa fa-search" aria-hidden="true"></i></button>
				
		</div>
		<div class="options-place">
			<?php
			if (isset($_SESSION['codusu'])) {
				echo
				'<div class="item-option"><i class="fa fa-user-circle-o" aria-hidden="true"></i><p>'.$_SESSION['nomusu'].'</p></div>';
			}else{
			?>
			<div class="item-option" title="Registrate"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
			<div class="item-option" title="Ingresar"><i class="fa fa-sign-in" aria-hidden="true"></i></div>
			<?php
			}
			?>
			<div class="item-option" title="Mis compras">
				<a href="carrito.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
			</div>
		</div>
	</header>
	<?php $codprod=$_GET["p"];$_SESSION['codpro']=$codprod;  ?>
	<div class="main-content">
		<div class="content-page">
			<section>
				<div class="part1">
					<img id="idimg" src="../assets/products/crepe.jpg">
				</div>
				<div class="part2">
					<h1>Modificar Producto</h1>
					<h2 id="idtitle">NOMBRE PRINCIPAL</h2>
					<h1 id="idprice">S/. 35.<span>99</span></h1>
					<h3 id="iddescription">Descripcion del producto</h3>
					<form action="../../controllers/producto/saveimg.php" method="POST"  enctype="multipart/form-data" target="_BLANK" >
				<input type="file" name="archivo">
				<button type="submit" name="saveImg" >Guardar imagen</button>
				</form>
				<form action="../../controllers/producto/modificar.php" method="POST">	
					<input type="text" name="nompro" placeholder="Nombre del producto" style="text-align:center;">
					<input  name="prepro" placeholder="Precio del producto" style="text-align:center;">
					<input type="text" name="barcode" placeholder="Codigo de Barras" style="text-align:center;">
					<input type="text" name="despro" placeholder="Descripcion" style="text-align:center;">
				<div>
					Unidades:
				<select type="text" name="unidades" style="text-align:center; margin: auto;">
					<option value="UNIDAD" selected>Unidad</option>
					<option value="PAQUETE">Paquete</option>
					<option value="CAJA">Caja</option>
					<option value="PESO">Peso</option>
				</select><br><br>
				Estado del producto
				<select type="text" name="status" style="text-align:center; margin: auto;">
					<option value="ENABLED" selected>En existencia</option>
					<option value="DISABLED">Sin Existencia</option>
				</select>
					<button type="submit" name="modProduct">Modificar producto</button>
						
					</form>
				</div>
			</section>
			<div class="title-section">Productos destacados</div>
			<div class="products-list" id="space-list">
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var p='<?php echo $_GET["p"]; ?>';
		 document.getElementById('codprod').innerHTML = p;
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
				url:'../../controllers/producto/merchantProduct.php',
				type:'POST',
				data:{},
				success:function(data){
					console.log(data);
					let html='';
					for (var i = 0; i < data.datos.length; i++) {
						if (data.datos[i].codpro==p) {
							document.getElementById("idimg").src="../assets/products/"+data.datos[i].rutimapro;
							document.getElementById("idtitle").innerHTML=data.datos[i].nompro;
							document.getElementById("idprice").innerHTML=formato_precio(data.datos[i].prepro);
							document.getElementById("iddescription").innerHTML=data.datos[i].despro;
						}
						html+=
						'<div class="product-box">'+
							'<a href="modproducto.php?p='+data.datos[i].codpro+'">'+
								'<div class="product">'+
									'<img src="../assets/products/'+data.datos[i].rutimapro+'">'+
									'<div class="detail-title">'+data.datos[i].nompro+'</div>'+
									'<div class="detail-description">'+data.datos[i].despro+'</div>'+
									'<div class="detail-price">'+formato_precio(data.datos[i].prepro)+
									'</div>'+
								'</div>'+
							'</a>'+
						'</div>';
					}
					document.getElementById("space-list").innerHTML=html;
				},
				error:function(err){
					console.error(err);
				}
			});
		});
		function formato_precio(valor){
			//10.99
			let svalor=valor.toString();
			let array=svalor.split(".");
			return "S/. "+array[0]+".<span>"+array[1]+"</span>";
		}
		function iniciar_compra(){
			$.ajax({
				url:'../controllers/compra/validar_inicio_compra.php',
				type:'POST',
				data:{
					codpro:p
				},
				success:function(data){
					console.log(data);
					if (data.state) {
						alert(data.detail);
					}else{
						alert(data.detail);
						if (data.open_login) {
							open_login();
						}
					}
				},
				error:function(err){
					console.error(err);
				}
			});
		}
		function open_login(){
			window.location.href="login.php";
		}
	</script>
</body>
</html>