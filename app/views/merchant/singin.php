

<!DOCTYPE html>
<html>
<head>
	<title>Mandadeo la comodidad del mandado</title>
	<meta charset="utf-8">
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Sen&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<style type="text/css">
		form{
			text-align: center;
			max-width: 460px;
			width: calc(100% - 40px);
			padding: 20px;
			background: #fff;
			border-radius: 5px;
			margin: auto;
		}
		form h3{
			margin: 5px 0;
		}
		form input{
			padding: 7px 10px;
			width: calc(100% - 22px);
			margin-bottom: 10px;
		}
		form button{
			padding: 10px 15px;
			width: calc(100%);
			background: var(--main-red);
			border: none;
			color: #fff;
		}
		form p{
			margin: 0;
			margin-bottom: 5px;
			color: var(--main-red);
			font-size: 14px;
		}
	</style>
</head>
<body>
	<header>
		<div class="logo-place"><a href="index.php"><img src="../assets/empresariallogo2.png"></a></div>
	</header>
	<div class="main-content">
		<div class="content-page">
			<form action="../../controllers/comerciante/singin.php" method="POST">
				<img src="../assets/empresariallogo1.png" alt="logo" width="135" height="89" style="margin-bottom: 85px;" >
				<h3>Registrarse<br>
				<br>
				<br></h3>
				<input type="text" name="username" placeholder="Nombre de usuario" style="text-align:center;">	
				<input type="text" name="emausu" placeholder="Correo Electronico" style="text-align:center;">
				<input type="text" name="merchrfc" placeholder="RFC" style="text-align:center;">
				<input type="text" name="merchdir" placeholder="Direccion" style="text-align:center;">
				<input type="password" id="pass"name="pasusu" placeholder="Contraseña" style="text-align:center;">
				<input type="password" id="cpass" name="confirmpasusu" placeholder="Confirmar contraseña" style="text-align:center; margin-bottom: 55px;">
				<button onclick="validarpass()" type="submit">Registrarme</button>
				<script>
					function validarpass() {
						if (document.getElementById('pass')!=document.getElementById('cpass')) {
							alert "Las contraseñas no coinciden";
						}
					}
				</script>
			</form>	
		</div>
	</div>
</body>
</html>
