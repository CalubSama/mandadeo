<?php
	session_start();
	unset($_SESSION['nombreImg']);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Mandadeo la comodidad del mandado</title>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Sen&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<link rel="stylesheet" href="../css/footercss.css">
</head>
<body>
	<header style="position: absolute; ">
		<div class="logo-place"><a href="index.php"><img src="../assets/empresariallogo2.png"></a></div>
		<div class="search-place">
			<input type="text" id="idbusqueda" placeholder="Encuenta todo lo que necesitas...">
			<button class="btn-main btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
		</div>
		<div class="options-place">
			<?php
			if (isset($_SESSION['codusu'])) {
				echo
				'<div class="item-option"><i class="fa fa-user-circle-o" aria-hidden="true"></i><p title="Usuario tipo comerciante">'.$_SESSION['emausu'].'</p></div>';
			
			}else{
			?>
			<div class="item-option" title="Registrate"><a href="singin.php"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a></div>
			<div class="item-option" title="Ingresar"><a href="login.php"><i class="fa fa-sign-in" aria-hidden="true"></i></a></div>
			<?php
			}
			?>
			<div aria-hidden="true" style="margin-left: 5px;"><a href="logout.php">	
			<img src="../assets/icons/logout.png" alt="Cerrar Sesión" weigth="27px" height="30px" style="margin-top: 5px;">
			</a>
			</div>
		</div>
	</header>
	<main>
		<div class="main-content">
		<div class="content-page">
			<div class="title-section" style="color:white; font-size: 30px; text-align: center; background-color: #DA2911; opacity: .7; margin-bottom: 5px; ">
				<div class="topnav" id="myTopnav">
				  <a href="#news">Productos</a>
				  | &nbsp <a href="pedido.php">Pedidos</a>
				</div>
			</div>
			<div class="utility" style="align-items: center;">	
			</div>
			<div style="  background-color:white; width: 100%; display: flex;">
				
				<form action="../../controllers/producto/saveimg.php" method="POST"  enctype="multipart/form-data" target="_BLANK"  style="background-color: white; 
				align-items: center;">
				<img src="../assets/products/nut1.png" alt="imagen producto" width="302" height="190" style=" text-align: center;" >
				<input type="file" name="archivo">
				<button type="submit" name="saveImg" >Guardar imagen</button>
				</form>
				<form action="../../controllers/producto/addProduct.php" method="POST"  style="background-color: white; 
				align-items: center;">
				<input type="text" name="nomprod" placeholder="Nombre del producto" style="text-align:center;">
				<input  name="prepro" placeholder="Precio del producto" style="text-align:center;">
				<input type="text" name="barcode" placeholder="Codigo de Barras" style="text-align:center;">
				<input type="text" name="despro" placeholder="Descripcion" style="text-align:center;">
				<div>
					Unidades:
				<select type="text" name="unidades" style="text-align:center; margin: auto;">
					<option value="UNIDAD" selected>Unidad</option>
					<option value="PAQUETE">Paquete</option>
					<option value="CAJA">Caja</option>
					<option value="PESO">Peso</option>
				</select><br><br>
				<button type="submit" name="addProduct">Añadir Producto</button>	
				</div>
				</form>

			</div>
			<div class="title-section" style="color:white; font-size: 30px; text-align: center;  margin-bottom: 5px; ">
					<h2>Mis Productos</h2>
				</div>
			<div class="products-list" id="space-list">
				
			</div>
		</div>
	</div>
	<!-- Mostrar los productos -->
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
				url:'../../controllers/producto/merchantProduct.php',
				type:'POST',
				data:{},
				success:function(data){
					console.log(data);
					let html='';
					for (var i = 0; i < data.datos.length; i++) {
						html+=
						'<div class="product-box">'+
							'<a href="modproducto.php?p='+data.datos[i].codpro+'">'+
								'<div class="product">'+
									'<img src="../assets/products/'+data.datos[i].rutimapro+'" style="	max-width: 216px;	max-height: 216px; min-width: 216px; min-height: 216px;">'+
									'<div class="detail-title">'+data.datos[i].nompro+'</div>'+
									'<div class="detail-description">'+data.datos[i].despro+'</div>'+
									'<div class="detail-price">'+formato_precio(data.datos[i].prepro)+'</div>'+
								'</div>'+
							'</a>'+
						'</div>';
					}
					document.getElementById("space-list").innerHTML=html;
				},
				error:function(err){
					console.error(err);
				}
			});
		});
		function formato_precio(valor){
			//10.99
			let svalor=valor.toString();
			let array=svalor.split(".");
			return "S/. "+array[0]+".<span>"+array[1]+"</span>";
		}
	</script>

	</main>
	<footer>
		<?php require '../footer.php'; ?>
	</footer>
	
</body>
</html>