<?php
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Mandadeo la comodidad del mandado</title>
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Sen&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" href="css/footercss.css">

</head>
<body>
	<header>
		<div class="logo-place"><a href="index.php"><img src="assets/logosencillo.blanco.png"></a></div>
		<div class="search-place">
			<form action="../controllers/producto/buscarProd.php" method="POST" style="display: flex; background-color: #DA2911; padding: 0px; ">
				<input type="text" id="idbusqueda" name="idbusqueda" placeholder="Encuenta todo lo que necesitas...">
				<button type="submit" class="btn-main btn-search" style="padding-top: 10px;    height: 40px;">	
					<i class="fa fa-search" aria-hidden="true"></i></button>	
			</form>
		</div>
		<div class="options-place">
			<?php
			if (isset($_SESSION['codusu'])) {
				echo
				'<div class="item-option"><i class="fa fa-user-circle-o" aria-hidden="true"></i><p>'.$_SESSION['nomusu'].'</p></div>';
			
			}else{
			?>
			<div class="item-option" title="Registrate"><a href="../views/singin.php"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a></div>
			<div class="item-option" title="Ingresar"><a href="../views/login.php"><i class="fa fa-sign-in" aria-hidden="true"></i></a></div>
			<?php
			}
			?>
			<div class="item-option" title="Mis compras">
				<a href="carrito.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
			</div>
			<div aria-hidden="true" style="margin-left: 5px;"><a href="logout.php">	
			<img src="assets/icons/logout.png" alt="Cerrar Sesión" weigth="27px" height="30px" style="margin-top: 5px;">
			</a>
			</div>
		</div>
	</header>

	<!-- CARRUCEL -->
	<?php require 'carrucel.php'; ?>
	<!-- //CARRUCEL -->
	<main>
			<div class="main-content">
		<div class="content-page">
			<div class="title-section" style="color:white; font-size: 30px; text-align: center;">Productos destacados</div>
			<div class="products-list" id="space-list">
			</div>
		</div>
	</div>
	<!-- Mostrar los productos -->
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax({
				url:'../controllers/producto/get_all_products.php',
				type:'POST',
				data:{},
				success:function(data){
					console.log(data);
					let html='';
					for (var i = 0; i < data.datos.length; i++) {
						html+=
						'<div class="product-box">'+
							'<a href="producto.php?p='+data.datos[i].codpro+'">'+
								'<div class="product">'+
									'<img src="assets/products/'+data.datos[i].rutimapro+'" style="	max-width: 216px;	max-height: 216px; min-width: 216px; min-height: 216px;">'+
									'<div class="detail-title">'+data.datos[i].nompro+'</div>'+
									'<div class="detail-description">'+data.datos[i].despro+'</div>'+
									'<div class="detail-price">'+formato_precio(data.datos[i].prepro)+'</div>'+
								'</div>'+
							'</a>'+
						'</div>';
					}
					document.getElementById("space-list").innerHTML=html;
				},
				error:function(err){
					console.error(err);
				}
			});
		});
		function formato_precio(valor){
			//10.99
			let svalor=valor.toString();
			let array=svalor.split(".");
			return "S/. "+array[0]+".<span>"+array[1]+"</span>";
		}
	</script>

	</main>
	<footer>
		<?php require 'footer.php'; ?>
	</footer>
	
</body>
</html>