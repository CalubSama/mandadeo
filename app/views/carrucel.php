<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="css/carousel.css">
</head>
<body>

<!-- banner -->
<div class="slide-contenedor">
        <div class="miSlider fade">
            <img src="assets/banner/banner2.jpg" alt="">
        </div>
        <div class="miSlider fade">
            <img src="assets/banner/banner1.jpg" alt="">
        </div>
        <div class="miSlider fade">
            <img src="assets/banner/banner3.jpg" alt="">
        </div>
        <div class="direcciones">
            <a href="#" class="atras" onclick="avanzaSlide(-1)">&#10094;</a>
            <a href="#" class="adelante" onclick="avanzaSlide(1)">&#10095;</a>
        </div>
        <div class="barras">
            <span class="barra active" onclick="posicionSlide(1)"></span>
            <span class="barra" onclick="posicionSlide(2)"></span>
            <span class="barra" onclick="posicionSlide(3)"></span>
        </div>
    </div>
    <script src="js/slider.js"></script>
</body>
</html>