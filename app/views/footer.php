<!DOCTYPE html>
<!-- code by webdevtrick ( https://webdevtrick.com ) -->
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />

	<title>Footer With Address And Phones</title>
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/footercss.css">

</head>

	<body>
		<footer class="footer-distributed">

			<div class="footer-left">

				<h3>Mandadeo <span>App Store</span></h3>

				<p class="footer-links">
					<a href="index.php">Home</a>
					·
					<a href="producto.php">Productos</a>
					·
					<a href="#">Sugerencias</a>
					·
					<a href="merchant/singin.php">Registrarse como vendedor</a>
					·
					<a href="merchant/login.php">Inicia Sesión como vendedor</a>
				</p>

				<p class="footer-company-name">Mandadeo &copy; 2020</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>Hermosillo Sonora</span> México</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>+(52 6623536630)</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">mandadeo@gmail.com</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>Información sobre nosotros</span>
					Mandadeo es una aplicacióon web destinada a apoyar a las microempresas dedicadas al giro comercial.
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

			</div>

		</footer>

	</body>

</html>